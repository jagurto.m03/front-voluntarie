import React , { useEffect, useState } from 'react'
import { io } from "socket.io-client";

const App = () => {

  const [id, setId] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const socket = io("https://api.juancarlosagurto.com.ar/",{
      transports: ['websocket'],
      jsonp: false,
    });

    setTimeout(() => {
      console.log(socket.id)
      if (socket.connected) {
        setId(socket.id)        
      } else setError("No estas en linea ......")

    },3000)
  },[])

  return (
    <div >
      { id ? <p>WS: {id}</p> : <p>{error}</p>  }
    </div>
  );
}

export default App;
